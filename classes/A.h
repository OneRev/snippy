#include <iostream>

using std::cout;

//A sample class for demonstration purposes...
// This shows some of the various things that can be put inside a class
 class A
 {
    private: 
	int var; //variables
    char  ch;
       

  	
     int fun();//prototype

     void morefun(int x)// complete function definition
	 {
		 x = 2 * x;
		 var = x;
	 }	 

     // symbolic constant 
     #define PI 3.14


    public:   

        bool do_this_get_zero = true;
        
	char fun2(int);

	void foo(char c )
	{
		cout<<"Foo"<<c<<"ish";
	}

       // A class inside a class!!!
       class B
       {
	    private:
		float tar;

            public:

		void bar(float f)
		{
		    tar = f;
		}

	      	   
	};// end of class B

 };//end of class A



